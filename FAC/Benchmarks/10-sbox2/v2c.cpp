#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;


int main() {
	string iFilePath;
	string oFilePath;
	string gateOp, gate, out, in1, in2;

	string homePath(getenv("HOME"));
	iFilePath = homePath + "/Downloads/isbox.v";
	oFilePath = "~/Downloads/sbox.cpp";

    ifstream iFile(iFilePath.c_str());
	if (!iFile.good()){
		cout << "\ncould not open file: isbox.v" << endl;
		//exit(9);
	}
    //ofstream oFile(oFilePath.c_str());
	
	cout << "bool compute(bool k0, bool k1, bool k2, bool k3, bool k4, bool k5, bool k6, bool k7){" << endl << endl;
	while(iFile.good()){
			//getline(iFile, gate);
			iFile >> gateOp;
			//cout << gateOp << endl << flush;
			iFile >> gate;
			//cout << gate << endl << flush;
			iFile >> out;
			//cout << out << endl << flush;
			iFile >> in1;
			//cout << in1 << endl << flush;
			iFile >> in2;
			//cout << in2 << endl << flush;
			
			if (gateOp == "XOR"){
				cout << "   bool " << out << " = " << in1 << " ";
				cout << '^';
				cout << " " << in2 << ";" << endl;
			} else if (gateOp == "XNOR"){
				cout <<  "   bool " << out << " = ~(" << in1 << " ";
				cout << '^';
				cout << " " << in2 << ");" << endl;
			} else if (gateOp == "AND"){
				cout <<  "   bool " << out << " = " << in1 << " ";
				cout << '&';
				cout << " " << in2 << ";" << endl;
			} 
	}
	cout << endl;
	cout << "   return (S0 - S1 - S2 - S3 - S4 - S5 - S6 - S7);" << endl;
	cout << "}" << endl;

			return 0;
}


