module GTECH_AND (output wire Z, input wire A, input wire B);
   and n1 (Z, A, B);
endmodule
   
module GTECH_XOR (output wire Z, input wire A, input wire B);
   xor n1 (Z, A, B);
endmodule
   
module GTECH_XNOR (output wire Z, input wire A, input wire B);
   xnor n1 (Z, A, B);
endmodule

module sbox_forward(input wire  [0:7] U,
		    output wire [0:7] S);

   wire [0:27] T;
   wire [0:63] M;
   wire [0:29] L;
   
   GTECH_XOR  G01(T[1],U[0],U[3]);
   GTECH_XOR  G02(T[2],U[0],U[5]);
   GTECH_XOR  G03(T[3],U[0],U[6]);
   GTECH_XOR  G04(T[4],U[3],U[5]);
   GTECH_XOR  G05(T[5],U[4],U[6]);
   GTECH_XOR  G06(T[6],T[1],T[5]);
   GTECH_XOR  G07(T[7],U[1],U[2]);
   GTECH_XOR  G08(T[8],U[7],T[6]);
   GTECH_XOR  G09(T[9],U[7],T[7]);
   GTECH_XOR  G10(T[10],T[6],T[7]);
   GTECH_XOR  G11(T[11],U[1],U[5]);
   GTECH_XOR  G12(T[12],U[2],U[5]);
   GTECH_XOR  G13(T[13],T[3],T[4]);
   GTECH_XOR  G14(T[14],T[6],T[11]);
   GTECH_XOR  G15(T[15],T[5],T[11]);
   GTECH_XOR  G16(T[16],T[5],T[12]);
   GTECH_XOR  G17(T[17],T[9],T[16]);
   GTECH_XOR  G18(T[18],U[3],U[7]);
   GTECH_XOR  G19(T[19],T[7],T[18]);
   GTECH_XOR  G20(T[20],T[1],T[19]);
   GTECH_XOR  G21(T[21],U[6],U[7]);
   GTECH_XOR  G22(T[22],T[7],T[21]);
   GTECH_XOR  G23(T[23],T[2],T[22]);
   GTECH_XOR  G24(T[24],T[2],T[10]);
   GTECH_XOR  G25(T[25],T[20],T[17]);
   GTECH_XOR  G26(T[26],T[3],T[16]);
   GTECH_XOR  G27(T[27],T[1],T[12]);
   GTECH_AND  G28(M[1],T[13],T[6]);
   GTECH_AND  G29(M[2],T[23],T[8]);
   GTECH_XOR  G30(M[3],T[14],M[1]);
   GTECH_AND  G31(M[4],T[19],U[7]);
   GTECH_XOR  G32(M[5],M[4],M[1]);
   GTECH_AND  G33(M[6],T[3],T[16]);
   GTECH_AND  G34(M[7],T[22],T[9]);
   GTECH_XOR  G35(M[8],T[26],M[6]);
   GTECH_AND  G36(M[9],T[20],T[17]);
   GTECH_XOR  G37(M[10],M[9],M[6]);
   GTECH_AND  G38(M[11],T[1],T[15]);
   GTECH_AND  G39(M[12],T[4],T[27]);
   GTECH_XOR  G40(M[13],M[12],M[11]);
   GTECH_AND  G41(M[14],T[2],T[10]);
   GTECH_XOR  G42(M[15],M[14],M[11]);
   GTECH_XOR  G43(M[16],M[3],M[2]);
   GTECH_XOR  G44(M[17],M[5],T[24]);
   GTECH_XOR  G45(M[18],M[8],M[7]);
   GTECH_XOR  G46(M[19],M[10],M[15]);
   GTECH_XOR  G47(M[20],M[16],M[13]);
   GTECH_XOR  G48(M[21],M[17],M[15]);
   GTECH_XOR  G49(M[22],M[18],M[13]);
   GTECH_XOR  G50(M[23],M[19],T[25]);
   GTECH_XOR  G51(M[24],M[22],M[23]);
   GTECH_AND  G52(M[25],M[22],M[20]);
   GTECH_XOR  G53(M[26],M[21],M[25]);
   GTECH_XOR  G54(M[27],M[20],M[21]);
   GTECH_XOR  G55(M[28],M[23],M[25]);
   GTECH_AND  G56(M[29],M[28],M[27]);
   GTECH_AND  G57(M[30],M[26],M[24]);
   GTECH_AND  G58(M[31],M[20],M[23]);
   GTECH_AND  G59(M[32],M[27],M[31]);
   GTECH_XOR  G60(M[33],M[27],M[25]);
   GTECH_AND  G61(M[34],M[21],M[22]);
   GTECH_AND  G62(M[35],M[24],M[34]);
   GTECH_XOR  G63(M[36],M[24],M[25]);
   GTECH_XOR  G64(M[37],M[21],M[29]);
   GTECH_XOR  G65(M[38],M[32],M[33]);
   GTECH_XOR  G66(M[39],M[23],M[30]);
   GTECH_XOR  G67(M[40],M[35],M[36]);
   GTECH_XOR  G68(M[41],M[38],M[40]);
   GTECH_XOR  G69(M[42],M[37],M[39]);
   GTECH_XOR  G70(M[43],M[37],M[38]);
   GTECH_XOR  G71(M[44],M[39],M[40]);
   GTECH_XOR  G72(M[45],M[42],M[41]);
   GTECH_AND  G73(M[46],M[44],T[6]);
   GTECH_AND  G74(M[47],M[40],T[8]);
   GTECH_AND  G75(M[48],M[39],U[7]);
   GTECH_AND  G76(M[49],M[43],T[16]);
   GTECH_AND  G77(M[50],M[38],T[9]);
   GTECH_AND  G78(M[51],M[37],T[17]);
   GTECH_AND  G79(M[52],M[42],T[15]);
   GTECH_AND  G80(M[53],M[45],T[27]);
   GTECH_AND  G81(M[54],M[41],T[10]);
   GTECH_AND  G82(M[55],M[44],T[13]);
   GTECH_AND  G83(M[56],M[40],T[23]);
   GTECH_AND  G84(M[57],M[39],T[19]);
   GTECH_AND  G85(M[58],M[43],T[3]);
   GTECH_AND  G86(M[59],M[38],T[22]);
   GTECH_AND  G87(M[60],M[37],T[20]);
   GTECH_AND  G88(M[61],M[42],T[1]);
   GTECH_AND  G89(M[62],M[45],T[4]);
   GTECH_AND  G90(M[63],M[41],T[2]);
   GTECH_XOR  G91(L[0],M[61],M[62]);
   GTECH_XOR  G92(L[1],M[50],M[56]);
   GTECH_XOR  G93(L[2],M[46],M[48]);
   GTECH_XOR  G94(L[3],M[47],M[55]);
   GTECH_XOR  G95(L[4],M[54],M[58]);
   GTECH_XOR  G96(L[5],M[49],M[61]);
   GTECH_XOR  G97(L[6],M[62],L[5]);
   GTECH_XOR  G98(L[7],M[46],L[3]);
   GTECH_XOR  G99(L[8],M[51],M[59]);
   GTECH_XOR  GA0(L[9],M[52],M[53]);
   GTECH_XOR  GA1(L[10],M[53],L[4]);
   GTECH_XOR  GA2(L[11],M[60],L[2]);
   GTECH_XOR  GA3(L[12],M[48],M[51]);
   GTECH_XOR  GA4(L[13],M[50],L[0]);
   GTECH_XOR  GA5(L[14],M[52],M[61]);
   GTECH_XOR  GA6(L[15],M[55],L[1]);
   GTECH_XOR  GA7(L[16],M[56],L[0]);
   GTECH_XOR  GA8(L[17],M[57],L[1]);
   GTECH_XOR  GA9(L[18],M[58],L[8]);
   GTECH_XOR  GB0(L[19],M[63],L[4]);
   GTECH_XOR  GB1(L[20],L[0],L[1]);
   GTECH_XOR  GB2(L[21],L[1],L[7]);
   GTECH_XOR  GB3(L[22],L[3],L[12]);
   GTECH_XOR  GB4(L[23],L[18],L[2]);
   GTECH_XOR  GB5(L[24],L[15],L[9]);
   GTECH_XOR  GB6(L[25],L[6],L[10]);
   GTECH_XOR  GB7(L[26],L[7],L[9]);
   GTECH_XOR  GB8(L[27],L[8],L[10]);
   GTECH_XOR  GB9(L[28],L[11],L[14]);
   GTECH_XOR  GC0(L[29],L[11],L[17]);
   GTECH_XOR  GC1(S[0],L[6],L[24]);
   GTECH_XNOR GC2(S[1],L[16],L[26]);
   GTECH_XNOR GC3(S[2],L[19],L[28]);
   GTECH_XOR  GC4(S[3],L[6],L[21]);
   GTECH_XOR  GC5(S[4],L[20],L[22]);
   GTECH_XOR  GC6(S[5],L[25],L[29]);
   GTECH_XNOR GC7(S[6],L[13],L[27]);
   GTECH_XNOR GC8(S[7],L[6],L[23]);

endmodule














 

















































































