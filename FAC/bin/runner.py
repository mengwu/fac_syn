#!/usr/bin/env python

import os;
import sys;
import shutil;

rtool = "r_tool.py"
java  = "java"
blank = " "

#inspect paths

def gen_ppts():
	command_string=rtool+blank+" --dump-points ."
	os.system(command_string);
	return ;

def do_instrumentation() :
	command_string = rtool+blank+" --run .";
	os.system(command_string);
	return;

def do_instrumentation_sp(sp_val) :
	command_string = rtool+blank+" --run_sp . "+str(sp_val);
	os.system(command_string);
	return;

def runner() :
	command_string = "inspect ./a.out"
	os.system(command_string);
	return;

def parser() :
	flag = False;
	for file in  os.listdir(".") :
		if os.path.isdir(file) and file.startswith("tr_gen"):
			flag = True;
			break

	if not flag:
		os.system("mkdir -p tr_gen");

	shutil.copy("program.dtrace","tr_gen/program.dtrace1");
	os.chdir("tr_gen");
	command_string = java+" -jar /home/ari/cvs/src/llvm-3.2.src/lib/Transforms/Daikon/trace_gen/parser.jar "+\
					" --filename program.dtrace1";
	os.system(command_string);
	os.system(" java daikon.Daikon *.dtrace > daikon_result");
	os.system (" cat daikon_result");

	return;
		
def cleanup():
	os.system("rm -rf program.dtrace");
	os.chdir("tr_gen")
	os.system("rm -rf *");
	os.system

def usage() :
	print "Enter <runner.py run> to run everything"
	print "Enter <runner.py run_sp <spcount #>> to run everything"
	print "Enter <runner.py comp> to gen ppts and compile "
	print "Enter <runner.py comp_sp <spcount #>> to run everything"
	print "Enter <runner.py comp_run> to just run the program not parser or daikon"
	print "Etner <runner.py clean> to clean previous trace"
	print "Enter <runner.py help> to get the help"


def main_controller() :

	if len(sys.argv) < 2 :
		usage();
		return ;

	if sys.argv[1] == "run":
		gen_ppts();
		do_instrumentation();
		runner();
		parser();

	elif sys.argv[1] == "run_sp":
		if len(sys.argv) < 3:
			usage();
			exit(0)

		else:
			sp_val = sys.argv[2];
			gen_ppts();
			do_instrumentation_sp(sp_val);
			runner();
			parser();


	elif sys.argv[1] == "gen_ppt":
		gen_ppts();

	elif sys.argv[1] == "comp":
		do_instrumentation();
	
	elif sys.argv[1] == "comp_sp":
		if len(sys.argv) < 3:
			usage();
			exit(0)

		else:
			sp_val = sys.argv[2];
			do_instrumentation_sp(sp_val);

	elif sys.argv[1] == "comp_run":
		runner();

	elif sys.argv[1] == "clean":
		cleanup();

	elif sys.arg[1] == "help":
		usage();

	return;

main_controller();

