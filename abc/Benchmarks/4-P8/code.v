
module compute( k1, k2, k3, r1, r2, r3, r4, ret);
  input  k1, k2, k3, r1, r2, r3, r4;
  output ret;


wire n00;
wire n01;
wire n02;
wire n03;
wire n04;
wire n05;
wire n06;
wire n07;
wire n08;
wire n09;
wire n10;
wire n11;
wire n12;
wire n13;
wire n14;
wire n15;
wire n16;
wire n17;
wire n18;
wire n19;
wire n20;
wire n21;
wire n22;
wire n23;
wire n24;
wire n25;
wire n26;
wire n27;
wire n28;
wire n29;
wire n30;
wire n31;
wire n32;


 assign  n31 = r3 ^ k3 ;
 assign  n29 = r1 ;
 assign  n28 = r2 ^ k2 ;
 assign  n27 = r3 ^ k3 ;
 assign  n25 = r1 ;
 assign  n24 = r2 ;
 assign  n21 = r2 ^ k2 ;
 assign  n20 = r3 ;
 assign  n19 = r1 ^ k1 ;
 assign  n18 = r3 ^ k3 ;
 assign  n17 = r3 ;
 assign  n16 = r2 ;
 assign  n15 = 1'b1 ^ n31 ;
 assign  n14 = n28 ^ r1 ;
 assign  n13 = 1'b0 ^ n27 ;
 assign  n12 = r2 ^ r1 ;
 assign  n11 = !1'b0 ;
 assign  n10 = r3 & n21 ;
 assign  n09 = n18 ^ n19 ;
 assign  n08 = r2 | r3 ;
 assign  n07 = n14 | n15 ;
 assign  n06 = n12 | n13 ;
 assign  n05 = n10 & n11 ;
 assign  n04 = n06 &  n07 ;
 assign  n03 = n09 ^  n04 ;
 assign  n02 = n03 ^  n05 ;
 assign  n01 = n02 ^  r4 ;
 assign  ret = n01 ^ n08 ;


endmodule 
