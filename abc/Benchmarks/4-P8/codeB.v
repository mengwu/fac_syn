// Benchmark "compute" written by ABC on Fri Oct 02 09:15:36 2015

module compute ( 
    k1, k2, k3, r1, r2, r3, r4,
    ret  );
  input  k1, k2, k3, r1, r2, r3, r4;
  output ret;
  wire n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
    n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
    n36, n37, n38, n39, n40, n41, n42;
  assign n8 = k3 & ~r3;
  assign n9 = ~k3 & r3;
  assign n10 = ~n8 & ~n9;
  assign n11 = k1 & ~r1;
  assign n12 = ~k1 & r1;
  assign n13 = ~n11 & ~n12;
  assign n14 = n10 & ~n13;
  assign n15 = ~n10 & n13;
  assign n16 = ~n14 & ~n15;
  assign n17 = r1 & ~r2;
  assign n18 = ~r1 & r2;
  assign n19 = ~n17 & ~n18;
  assign n20 = n10 & n19;
  assign n21 = k2 & ~r2;
  assign n22 = ~k2 & r2;
  assign n23 = ~n21 & ~n22;
  assign n24 = r1 & n23;
  assign n25 = ~r1 & ~n23;
  assign n26 = ~n10 & ~n25;
  assign n27 = ~n24 & n26;
  assign n28 = ~n20 & ~n27;
  assign n29 = n16 & n28;
  assign n30 = ~n16 & ~n28;
  assign n31 = ~n29 & ~n30;
  assign n32 = r3 & ~n23;
  assign n33 = n31 & n32;
  assign n34 = ~n31 & ~n32;
  assign n35 = ~n33 & ~n34;
  assign n36 = r4 & n35;
  assign n37 = ~r4 & ~n35;
  assign n38 = ~n36 & ~n37;
  assign n39 = ~r2 & ~r3;
  assign n40 = n38 & ~n39;
  assign n41 = ~n38 & n39;
  assign n42 = ~n40 & ~n41;
  assign ret = ~n42;
endmodule


