
module compute( k1, k2, k3, r1, r2, r3, r4, ret);
  input  k1, k2, k3, r1, r2, r3, r4;
  output ret;


  wire n01;
  wire n02;
  wire n03;
  wire n04;
  wire n05;
  wire n06;
  wire n07;
  wire n08;
  wire n09;
  wire n10;
  wire n11;
  wire n12;
  wire n13;
  wire n14;
  wire n15;
  wire n16;
  wire t1;
  wire t2;
  wire t3;
  wire t4;
  wire t5;
  wire t6;


  assign  n01 = !r2;
  assign  t1 = n01 & r3;


  assign  n04 = k3 ^ r3;
  assign  t2 = r2 & n04;

  assign  t3 = r1 ^ 1'b0; 

  assign  t4 = k1 ^ r1;

  assign  n07 = k2 ^ r2;
  assign  n08 = !n07;
  assign  n09 = k3 ^ r3;
  assign  t5 = n08 & n09;

  assign  n12 = k2 ^ r2;
  assign  t6 = n12 & r3;

  assign  n14 = t2 ^ t6;
  assign  n03 = n14 ^ r4;

  assign  n15 = t1 ^ t5;
  assign  n11 = n03 ^ n15;
  assign  n16 = n11 ^ t4;


  assign  ret = n16 ^ t3;

endmodule 