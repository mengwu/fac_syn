// Benchmark "compute" written by ABC on Thu Oct 01 20:59:39 2015

module compute ( 
    k1, k2, k3, r1, r2, r3, r4,
    ret  );
  input  k1, k2, k3, r1, r2, r3, r4;
  output ret;
  wire n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
    n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
    n36, n37, n38;
  assign n8 = ~k3 & r3;
  assign n9 = k3 & ~r3;
  assign n10 = ~n8 & ~n9;
  assign n11 = r2 & ~n10;
  assign n12 = ~k2 & r2;
  assign n13 = k2 & ~r2;
  assign n14 = ~n12 & ~n13;
  assign n15 = r3 & ~n14;
  assign n16 = ~n11 & n15;
  assign n17 = n11 & ~n15;
  assign n18 = ~n16 & ~n17;
  assign n19 = r4 & n18;
  assign n20 = ~r4 & ~n18;
  assign n21 = ~n19 & ~n20;
  assign n22 = ~r2 & r3;
  assign n23 = ~n10 & n14;
  assign n24 = ~n22 & n23;
  assign n25 = n22 & ~n23;
  assign n26 = ~n24 & ~n25;
  assign n27 = n21 & ~n26;
  assign n28 = ~n21 & n26;
  assign n29 = ~n27 & ~n28;
  assign n30 = ~k1 & r1;
  assign n31 = k1 & ~r1;
  assign n32 = ~n30 & ~n31;
  assign n33 = n29 & ~n32;
  assign n34 = ~n29 & n32;
  assign n35 = ~n33 & ~n34;
  assign n36 = r1 & n35;
  assign n37 = ~r1 & ~n35;
  assign n38 = ~n36 & ~n37;
  assign ret = ~n38;
endmodule


