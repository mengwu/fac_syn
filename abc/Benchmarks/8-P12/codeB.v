// Benchmark "compute" written by ABC on Sun Oct 11 08:28:55 2015

module compute ( 
    k1, k2, k3, k4, k5, k6, k7, k8,
    \cp1[0] , \cp1[1] , \cp1[2] , \cp1[3] , \qp2[0] , \qp2[1] , \qp2[2] ,
    \qp2[3] , \qp3[0] , \qp3[1] , \qp3[2] , \qp3[3]   );
  input  k1, k2, k3, k4, k5, k6, k7, k8;
  output \cp1[0] , \cp1[1] , \cp1[2] , \cp1[3] , \qp2[0] , \qp2[1] , \qp2[2] ,
    \qp2[3] , \qp3[0] , \qp3[1] , \qp3[2] , \qp3[3] ;
  wire n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
    n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
    n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61,
    n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
    n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89,
    n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102,
    n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114,
    n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126,
    n127, n128, n129, n130, n131, n132, n133, n134, n135, n136, n137, n138,
    n139, n140, n141, n142, n143, n144, n145, n146, n147;
  assign n20 = k2 & k3;
  assign n21 = ~k2 & k3;
  assign n22 = k2 & ~k3;
  assign n23 = ~n21 & ~n22;
  assign n24 = k4 & n23;
  assign n25 = ~k4 & ~n23;
  assign n26 = ~n24 & ~n25;
  assign n27 = k4 & n20;
  assign n28 = n26 & n27;
  assign n29 = ~n26 & ~n27;
  assign n30 = ~n28 & ~n29;
  assign n31 = k1 & n30;
  assign n32 = ~k1 & ~n30;
  assign n33 = ~n31 & ~n32;
  assign n34 = k1 & k3;
  assign n35 = n33 & n34;
  assign n36 = ~n33 & ~n34;
  assign n37 = ~n35 & ~n36;
  assign n38 = n20 & n37;
  assign n39 = ~n20 & ~n37;
  assign n40 = ~n38 & ~n39;
  assign n41 = k1 & k2;
  assign n42 = k3 & n41;
  assign n43 = n40 & n42;
  assign n44 = ~n40 & ~n42;
  assign n45 = ~n43 & ~n44;
  assign n46 = k2 & k4;
  assign n47 = n34 & ~n41;
  assign n48 = ~n34 & n41;
  assign n49 = ~n47 & ~n48;
  assign n50 = n20 & n49;
  assign n51 = ~n20 & ~n49;
  assign n52 = ~n50 & ~n51;
  assign n53 = k4 & n52;
  assign n54 = ~k4 & ~n52;
  assign n55 = ~n53 & ~n54;
  assign n56 = n46 & n55;
  assign n57 = ~n46 & ~n55;
  assign n58 = ~n56 & ~n57;
  assign n59 = k4 & n41;
  assign n60 = n58 & n59;
  assign n61 = ~n58 & ~n59;
  assign n62 = ~n60 & ~n61;
  assign n63 = k3 & ~n41;
  assign n64 = ~k3 & n41;
  assign n65 = ~n63 & ~n64;
  assign n66 = n34 & n65;
  assign n67 = ~n34 & ~n65;
  assign n68 = ~n66 & ~n67;
  assign n69 = k4 & n68;
  assign n70 = ~k4 & ~n68;
  assign n71 = ~n69 & ~n70;
  assign n72 = k1 & k4;
  assign n73 = n71 & n72;
  assign n74 = ~n71 & ~n72;
  assign n75 = ~n73 & ~n74;
  assign n76 = k4 & n34;
  assign n77 = n75 & n76;
  assign n78 = ~n75 & ~n76;
  assign n79 = ~n77 & ~n78;
  assign n80 = n30 & n72;
  assign n81 = ~n30 & ~n72;
  assign n82 = ~n80 & ~n81;
  assign n83 = n46 & n82;
  assign n84 = ~n46 & ~n82;
  assign n85 = ~n83 & ~n84;
  assign n86 = k3 & k4;
  assign n87 = n85 & n86;
  assign n88 = ~n85 & ~n86;
  assign n89 = ~n87 & ~n88;
  assign n90 = k1 & k5;
  assign n91 = k4 & k6;
  assign n92 = ~n90 & n91;
  assign n93 = n90 & ~n91;
  assign n94 = ~n92 & ~n93;
  assign n95 = k3 & k7;
  assign n96 = n94 & n95;
  assign n97 = ~n94 & ~n95;
  assign n98 = ~n96 & ~n97;
  assign n99 = k2 & k8;
  assign n100 = n98 & n99;
  assign n101 = ~n98 & ~n99;
  assign n102 = ~n100 & ~n101;
  assign n103 = k2 & k5;
  assign n104 = ~k1 & k4;
  assign n105 = k1 & ~k4;
  assign n106 = ~n104 & ~n105;
  assign n107 = k6 & ~n106;
  assign n108 = ~n103 & n107;
  assign n109 = n103 & ~n107;
  assign n110 = ~n108 & ~n109;
  assign n111 = ~k3 & k4;
  assign n112 = k3 & ~k4;
  assign n113 = ~n111 & ~n112;
  assign n114 = k7 & ~n113;
  assign n115 = n110 & n114;
  assign n116 = ~n110 & ~n114;
  assign n117 = ~n115 & ~n116;
  assign n118 = k8 & ~n23;
  assign n119 = n117 & n118;
  assign n120 = ~n117 & ~n118;
  assign n121 = ~n119 & ~n120;
  assign n122 = k3 & k5;
  assign n123 = k2 & k6;
  assign n124 = ~n122 & n123;
  assign n125 = n122 & ~n123;
  assign n126 = ~n124 & ~n125;
  assign n127 = k7 & ~n106;
  assign n128 = n126 & n127;
  assign n129 = ~n126 & ~n127;
  assign n130 = ~n128 & ~n129;
  assign n131 = k8 & ~n113;
  assign n132 = n130 & n131;
  assign n133 = ~n130 & ~n131;
  assign n134 = ~n132 & ~n133;
  assign n135 = k4 & k5;
  assign n136 = k3 & k6;
  assign n137 = ~n135 & n136;
  assign n138 = n135 & ~n136;
  assign n139 = ~n137 & ~n138;
  assign n140 = k2 & k7;
  assign n141 = n139 & n140;
  assign n142 = ~n139 & ~n140;
  assign n143 = ~n141 & ~n142;
  assign n144 = k8 & ~n106;
  assign n145 = n143 & n144;
  assign n146 = ~n143 & ~n144;
  assign n147 = ~n145 & ~n146;
  assign \cp1[0]  = ~n45;
  assign \cp1[1]  = ~n62;
  assign \cp1[2]  = ~n79;
  assign \cp1[3]  = ~n89;
  assign \qp2[0]  = ~n102;
  assign \qp2[1]  = ~n121;
  assign \qp2[2]  = ~n134;
  assign \qp2[3]  = ~n147;
  assign \qp3[0]  = ~n102;
  assign \qp3[1]  = ~n121;
  assign \qp3[2]  = ~n134;
  assign \qp3[3]  = ~n147;
endmodule


