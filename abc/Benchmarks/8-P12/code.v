
module compute( k1, k2, k3, k4, k5, k6, k7, k8, cp1, qp2, qp3);
  input  k1, k2, k3, k4, k5, k6, k7, k8;
  output [3:0] cp1, qp2, qp3;

wire aAp1;
wire aAp2;
wire aBp2;
wire aAp3;
wire aBp3;




   assign  aAp1 = k2 ^ k3 ^ k4 ^ (k2 & k3 & k4);
   assign  cp1[0] = aAp1 ^ k1 ^ (k1 & k3) ^ (k2 & k3) ^ (k1 & k2 & k3);
   assign  cp1[1] = (k1 & k2) ^ (k1 & k3) ^ (k2 & k3)   ^              k4 ^ (k2 & k4) ^ (k1 & k2 & k4);
   assign  cp1[2] = (k1 & k2) ^ k3 ^ (k1 & k3) ^ k4 ^              (k1 & k4) ^ (k1 & k3 & k4);
   assign  cp1[3] = aAp1 ^ (k1 & k4) ^ (k2 & k4) ^ (k3 & k4);




    assign  aAp2 = k1 ^ k4;
    assign  aBp2 = k3 ^ k4;
    assign  qp2[0] = (k1 & k5) ^ (k4 & k6) ^ (k3 & k7) ^  (k2 & k8);
    assign  qp2[1] = (k2 & k5) ^ (aAp2   & k6) ^ (aBp2   & k7) ^ ((k2 ^ k3) & k8);
    assign  qp2[2] = (k3 & k5) ^ (k2 & k6) ^ (aAp2   & k7) ^  (aBp2 & k8);
    assign  qp2[3] = (k4 & k5) ^ (k3 & k6) ^ (k2 & k7) ^  (aAp2 & k8);




    assign  aAp3 = k1 ^ k4;
    assign  aBp3 = k3 ^ k4;
    assign  qp3[0] = (k1 & k5) ^ (k4 & k6) ^ (k3 & k7) ^  (k2 & k8);
    assign  qp3[1] = (k2 & k5) ^ (aAp3   & k6) ^ (aBp3   & k7) ^ ((k2 ^ k3) & k8);
    assign  qp3[2] = (k3 & k5) ^ (k2 & k6) ^ (aAp3   & k7) ^  (aBp3 & k8);
    assign  qp3[3] = (k4 & k5) ^ (k3 & k6) ^ (k2 & k7) ^  (aAp3 & k8);



endmodule 

