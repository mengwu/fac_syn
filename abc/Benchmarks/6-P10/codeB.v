// Benchmark "compute" written by ABC on Fri Oct 02 10:44:12 2015

module compute ( 
    k1, k2, k3, k4,
    \c[0] , \c[1] , \c[2] , \c[3]   );
  input  k1, k2, k3, k4;
  output \c[0] , \c[1] , \c[2] , \c[3] ;
  wire n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
    n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
    n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
    n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63,
    n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77;
  assign n8 = k2 & k3;
  assign n9 = ~k2 & k3;
  assign n10 = k2 & ~k3;
  assign n11 = ~n9 & ~n10;
  assign n12 = k4 & n11;
  assign n13 = ~k4 & ~n11;
  assign n14 = ~n12 & ~n13;
  assign n15 = k4 & n8;
  assign n16 = n14 & n15;
  assign n17 = ~n14 & ~n15;
  assign n18 = ~n16 & ~n17;
  assign n19 = k1 & n18;
  assign n20 = ~k1 & ~n18;
  assign n21 = ~n19 & ~n20;
  assign n22 = k1 & k3;
  assign n23 = n21 & n22;
  assign n24 = ~n21 & ~n22;
  assign n25 = ~n23 & ~n24;
  assign n26 = n8 & n25;
  assign n27 = ~n8 & ~n25;
  assign n28 = ~n26 & ~n27;
  assign n29 = k1 & k2;
  assign n30 = k3 & n29;
  assign n31 = n28 & n30;
  assign n32 = ~n28 & ~n30;
  assign n33 = ~n31 & ~n32;
  assign n34 = k2 & k4;
  assign n35 = n22 & ~n29;
  assign n36 = ~n22 & n29;
  assign n37 = ~n35 & ~n36;
  assign n38 = n8 & n37;
  assign n39 = ~n8 & ~n37;
  assign n40 = ~n38 & ~n39;
  assign n41 = k4 & n40;
  assign n42 = ~k4 & ~n40;
  assign n43 = ~n41 & ~n42;
  assign n44 = n34 & n43;
  assign n45 = ~n34 & ~n43;
  assign n46 = ~n44 & ~n45;
  assign n47 = k4 & n29;
  assign n48 = n46 & n47;
  assign n49 = ~n46 & ~n47;
  assign n50 = ~n48 & ~n49;
  assign n51 = k3 & ~n29;
  assign n52 = ~k3 & n29;
  assign n53 = ~n51 & ~n52;
  assign n54 = n22 & n53;
  assign n55 = ~n22 & ~n53;
  assign n56 = ~n54 & ~n55;
  assign n57 = k4 & n56;
  assign n58 = ~k4 & ~n56;
  assign n59 = ~n57 & ~n58;
  assign n60 = k1 & k4;
  assign n61 = n59 & n60;
  assign n62 = ~n59 & ~n60;
  assign n63 = ~n61 & ~n62;
  assign n64 = k4 & n22;
  assign n65 = n63 & n64;
  assign n66 = ~n63 & ~n64;
  assign n67 = ~n65 & ~n66;
  assign n68 = n18 & n60;
  assign n69 = ~n18 & ~n60;
  assign n70 = ~n68 & ~n69;
  assign n71 = n34 & n70;
  assign n72 = ~n34 & ~n70;
  assign n73 = ~n71 & ~n72;
  assign n74 = k3 & k4;
  assign n75 = n73 & n74;
  assign n76 = ~n73 & ~n74;
  assign n77 = ~n75 & ~n76;
  assign \c[0]  = ~n33;
  assign \c[1]  = ~n50;
  assign \c[2]  = ~n67;
  assign \c[3]  = ~n77;
endmodule


