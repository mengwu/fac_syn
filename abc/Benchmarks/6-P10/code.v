
module compute( k1, k2, k3, k4, c);
  input  k1, k2, k3, k4;
  output [3:0] c;


wire aA;


   assign  aA = k2 ^ k3 ^ k4 ^ (k2 & k3 & k4);
   assign  c[0] = aA ^ k1 ^ (k1 & k3) ^ (k2 & k3) ^ (k1 & k2 & k3);
   assign  c[1] = (k1 & k2) ^ (k1 & k3) ^ (k2 & k3)   ^              k4 ^ (k2 & k4) ^ (k1 & k2 & k4);
   assign  c[2] = (k1 & k2) ^ k3 ^ (k1 & k3) ^ k4 ^              (k1 & k4) ^ (k1 & k3 & k4);
   assign  c[3] = aA ^ (k1 & k4) ^ (k2 & k4) ^ (k3 & k4);



endmodule 
