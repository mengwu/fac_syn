// Benchmark "compute" written by ABC on Fri Oct 02 09:26:46 2015

module compute ( 
    k1, k2, k3,
    ret  );
  input  k1, k2, k3;
  output ret;
  wire n4, n5, n6, n7;
  assign n4 = ~k2 & k3;
  assign n5 = ~k1 & n4;
  assign n6 = k1 & ~n4;
  assign n7 = ~n5 & ~n6;
  assign ret = ~n7;
endmodule


