
module compute( k1, k2, k3, ret);
  input  k1, k2, k3;
  output ret;



assign  ret = k1 ^ ( (!k2) & k3 );


endmodule 