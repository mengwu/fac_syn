// Benchmark "compute" written by ABC on Fri Oct 02 09:08:29 2015

module compute ( 
    k1, k2, k3, r1, r2, r3, r4,
    ret  );
  input  k1, k2, k3, r1, r2, r3, r4;
  output ret;
  wire n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
    n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
    n36, n37, n38, n39, n40, n41, n42;
  assign n8 = ~r2 & ~r3;
  assign n9 = k3 & ~r3;
  assign n10 = ~k3 & r3;
  assign n11 = ~n9 & ~n10;
  assign n12 = k1 & ~r1;
  assign n13 = ~k1 & r1;
  assign n14 = ~n12 & ~n13;
  assign n15 = n11 & ~n14;
  assign n16 = ~n11 & n14;
  assign n17 = ~n15 & ~n16;
  assign n18 = n8 & ~n17;
  assign n19 = ~n8 & n17;
  assign n20 = ~n18 & ~n19;
  assign n21 = r1 & ~r2;
  assign n22 = ~r1 & r2;
  assign n23 = ~n21 & ~n22;
  assign n24 = n11 & n23;
  assign n25 = k2 & ~r2;
  assign n26 = ~k2 & r2;
  assign n27 = ~n25 & ~n26;
  assign n28 = r1 & n27;
  assign n29 = ~r1 & ~n27;
  assign n30 = ~n11 & ~n29;
  assign n31 = ~n28 & n30;
  assign n32 = ~n24 & ~n31;
  assign n33 = n20 & n32;
  assign n34 = ~n20 & ~n32;
  assign n35 = ~n33 & ~n34;
  assign n36 = r4 & n35;
  assign n37 = ~r4 & ~n35;
  assign n38 = ~n36 & ~n37;
  assign n39 = r3 & ~n27;
  assign n40 = n38 & n39;
  assign n41 = ~n38 & ~n39;
  assign n42 = ~n40 & ~n41;
  assign ret = ~n42;
endmodule


