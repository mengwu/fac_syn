
module compute( k1, k2, k3, k4, k5, k6, k7, k8, q);
  input  k1, k2, k3, k4, k5, k6, k7, k8;
  output [3:0] q;


wire aA;
wire aB;


    assign  aA = k1 ^ k4;
    assign  aB = k3 ^ k4;
    assign  q[0] = (k1 & k5) ^ (k4 & k6) ^ (k3 & k7) ^  (k2 & k8);
    assign  q[1] = (k2 & k5) ^ (aA   & k6) ^ (aB   & k7) ^ ((k2 ^ k3) & k8);
    assign  q[2] = (k3 & k5) ^ (k2 & k6) ^ (aA   & k7) ^  (aB & k8);
    assign  q[3] = (k4 & k5) ^ (k3 & k6) ^ (k2 & k7) ^  (aA & k8);



endmodule 
