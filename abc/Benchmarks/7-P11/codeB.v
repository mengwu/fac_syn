// Benchmark "compute" written by ABC on Fri Oct 02 14:56:07 2015

module compute ( 
    k1, k2, k3, k4, k5, k6, k7, k8,
    \q[0] , \q[1] , \q[2] , \q[3]   );
  input  k1, k2, k3, k4, k5, k6, k7, k8;
  output \q[0] , \q[1] , \q[2] , \q[3] ;
  wire n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25,
    n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
    n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53,
    n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67,
    n68, n69, n70, n71, n72;
  assign n12 = k1 & k5;
  assign n13 = k4 & k6;
  assign n14 = ~n12 & n13;
  assign n15 = n12 & ~n13;
  assign n16 = ~n14 & ~n15;
  assign n17 = k3 & k7;
  assign n18 = n16 & n17;
  assign n19 = ~n16 & ~n17;
  assign n20 = ~n18 & ~n19;
  assign n21 = k2 & k8;
  assign n22 = n20 & n21;
  assign n23 = ~n20 & ~n21;
  assign n24 = ~n22 & ~n23;
  assign n25 = k2 & k5;
  assign n26 = ~k1 & k4;
  assign n27 = k1 & ~k4;
  assign n28 = ~n26 & ~n27;
  assign n29 = k6 & ~n28;
  assign n30 = ~n25 & n29;
  assign n31 = n25 & ~n29;
  assign n32 = ~n30 & ~n31;
  assign n33 = ~k3 & k4;
  assign n34 = k3 & ~k4;
  assign n35 = ~n33 & ~n34;
  assign n36 = k7 & ~n35;
  assign n37 = n32 & n36;
  assign n38 = ~n32 & ~n36;
  assign n39 = ~n37 & ~n38;
  assign n40 = ~k2 & k3;
  assign n41 = k2 & ~k3;
  assign n42 = ~n40 & ~n41;
  assign n43 = k8 & ~n42;
  assign n44 = n39 & n43;
  assign n45 = ~n39 & ~n43;
  assign n46 = ~n44 & ~n45;
  assign n47 = k3 & k5;
  assign n48 = k2 & k6;
  assign n49 = ~n47 & n48;
  assign n50 = n47 & ~n48;
  assign n51 = ~n49 & ~n50;
  assign n52 = k7 & ~n28;
  assign n53 = n51 & n52;
  assign n54 = ~n51 & ~n52;
  assign n55 = ~n53 & ~n54;
  assign n56 = k8 & ~n35;
  assign n57 = n55 & n56;
  assign n58 = ~n55 & ~n56;
  assign n59 = ~n57 & ~n58;
  assign n60 = k4 & k5;
  assign n61 = k3 & k6;
  assign n62 = ~n60 & n61;
  assign n63 = n60 & ~n61;
  assign n64 = ~n62 & ~n63;
  assign n65 = k2 & k7;
  assign n66 = n64 & n65;
  assign n67 = ~n64 & ~n65;
  assign n68 = ~n66 & ~n67;
  assign n69 = k8 & ~n28;
  assign n70 = n68 & n69;
  assign n71 = ~n68 & ~n69;
  assign n72 = ~n70 & ~n71;
  assign \q[0]  = ~n24;
  assign \q[1]  = ~n46;
  assign \q[2]  = ~n59;
  assign \q[3]  = ~n72;
endmodule


